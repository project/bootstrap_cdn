<?php

/**
 * @file
 * Administrative page callbacks for the Bootstrap CDN module.
 */

/**
 * Implementation of hook_admin_settings() for configuring the module
 *
 * @param array $form_state
 *   structure associative drupal form array
 * @return array
 */
function bootstrap_cdn_admin_settings_form() {
  $form['bootstrap_cdn'] = array(
    '#type' => 'vertical_tabs',
  );

  // General Settings
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
    '#group' => 'bootstrap_cdn',
  );
  $form['general']['bootstrap_cdn_js'] = array(
    '#title' => t('Bootstrap JS'),
    '#description' => t('Add bootstrap JS via CDN.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bootstrap_cdn_js', FALSE),
  );
  $form['general']['bootstrap_cdn_css'] = array(
    '#title' => t('Bootstrap CSS'),
    '#description' => t('Add bootstrap CSS via CDN.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bootstrap_cdn_css', FALSE),
  );

  // Role specific visibility configurations.
  $form['role_visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role specific script settings'),
    '#collapsible' => TRUE,
    '#group' => 'bootstrap_cdn',
  );

  $roles = user_roles();
  $role_options = array();
  foreach ($roles as $rid => $name) {
    $role_options[$rid] = $name;
  }
  $form['role_visibility']['bootstrap_cdn_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Remove script for specific roles'),
    '#default_value' => variable_get('bootstrap_cdn_roles', array()),
    '#options' => $role_options,
    '#description' => t('Remove script only for the selected role(s). If none of the roles are selected, all roles will have the script. Otherwise, any roles selected here will NOT have the script.'),
  );

  // Page specific visibility configurations.
  $form['page_visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific script settings'),
    '#collapsible' => TRUE,
    '#group' => 'bootstrap_cdn',
  );

  $access = user_access('use PHP for Bootstrap CDN visibility');
  $visibility = variable_get('bootstrap_cdn_visibility', 0);
  $pages = variable_get('bootstrap_cdn_pages', '');

  if ($visibility == 2 && !$access) {
    $form['page_visibility'] = array();
    $form['page_visibility']['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['page_visibility']['pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(t('Add to every page except the listed pages.'), t('Add to the listed pages only.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
    if ($access) {
      $options[] = t('Add if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= ' '. t('If the PHP-mode is chosen, enter PHP code between %php tags. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    $form['page_visibility']['bootstrap_cdn_visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Add script to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['page_visibility']['bootstrap_cdn_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => $pages,
      '#description' => $description,
      '#wysiwyg' => FALSE,
    );
  }

  return system_settings_form($form);
}
